import java.util.Scanner;

public class AtWhatCost {
    public static void main(String[] args) {

        System.out.println("Are you awake? (Y/N):");
        Scanner scanner = new Scanner(System.in);
        String userText = scanner.nextLine();

        if (userText.contains("Y") || userText.contains("y")) {
            System.out.println("BUT AT WHAT COST?!");
        } else if (userText.contains("N") || (userText.contains("n"))) {
            System.out.println("Good. Keep sleeping!");
        } else {
            System.out.println("I don't understand. ARE YOU AWAKE?! (Y/N):");
        }


    }
}

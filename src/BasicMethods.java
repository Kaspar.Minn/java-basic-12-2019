public class BasicMethods {
    public static void main (String[] args) {

        System.out.println("Exercise 3! Let's do it.");


        System.out.println("Toote hind maksuta: " + 100);
        System.out.println("Toote hind maksuga: " + addVat(100));

        System.out.println("Toote hind maksuta: " + 65.88);
        System.out.println("Toote hind maksuga: " + addVat(65.88));

        System.out.println("Exercise 4 ... ");
        // Isikukoodi harjutus.

        System.out.println("Inimene isikukoodiga 49403136526 on..."); extractPersonGender(("49403136526"));

    }

    public static double addVat(double price) {
        return 1.2 * price;
    }

    public static String extractPersonGender(String personCode) {

        /* ------------------------------------------------------

        // System.out.println((Integer.parseInt("" + personCode.charAt(0)));
        personCode.substring(0, 1); // "49403136526" --> "4"

        String genderDigit = personCode.substring(0, 1);

        if (genderDigit.equals("1") || genderDigit.equals("3") || genderDigit.equals("5") || genderDigit.equals("7")) {
            return "M";
        } else {
            return "F";
        }

        ------------------------------------------------------ */

        int genderDigit = Integer.parseInt(personCode.substring(0, 1));
        return genderDigit % 2 == 0 ? "F" : "M";

    }

    public static int extractYear(String personCode) {
        int genderDigit = Integer.parseInt(personCode.substring(0,1));
        int genderYearDigits = Integer.parseInt(personCode.substring(1, 3));

        switch (genderDigit) {

            case 1:
                return 1700 + genderYearDigits;
            case 2:
                return 1800 + genderYearDigits;
            case 3:
                return 1900 + genderYearDigits;
            case 4:
                return 2000 + genderYearDigits;
            case 5:
                return 2100 + genderYearDigits;
            case 6:
                return 2200 + genderYearDigits;
            case 7:
                return 2300 + genderYearDigits;
            case 8:
                return 2400 + genderYearDigits;
            default:
                return -1;
        }



    }
}

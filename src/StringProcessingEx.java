public class StringProcessingEx {
    public static void main(String[] args) {

        System.out.println("Hello, World!");

        String s1 = "Hola, el Worlde!";
        System.out.println(s1);

        String s2 = "Steven Hawking once said: \"Life would be tragic if it weren\'t funny.\"";
        System.out.println(s2);

        String s3 = "Kui liita kokku sõned \"See on teksti esimene pool  \" ning \"See on teksti teine pool\", siis tulemuseks saame \"See on teksti esimene pool See on teksti teine pool\".";
        System.out.println(s3);

        int tallinnPop = 450_000;
        String s4 = "Tallinas elab " + tallinnPop + " inimest.";
        System.out.println(s4);

        String tallinnPop2 = String.format("Tallinnas elab %,d inimest.", tallinnPop);
        System.out.println(tallinnPop2);

        int number = 6;
        String can3 = (number % 3 == 0) ?
                "Yes, it divides!" :
                "No, it does not divide...";

        System.out.println(can3);

        String planet1 = "Merkuur";
        String planet2 = "Venus";
        String planet3 = "Maa";
        String planet4 = "Marss";
        String planet5 = "Jupiter";
        String planet6 = "Saturn";
        String planet7 = "Uran";
        String planet8 = "Neptuun";
        int planetCount = 8;

        String planetNames = String.format("%s, %s, %s, %s, %s, %s, %s ja %s on Päikesesüsteemi %d planeeti.",
                planet1, planet2, planet3, planet4, planet5, planet6, planet7, planet8, planetCount);

        // Prindib kupatuse:
        System.out.println(planetNames);

    }
}

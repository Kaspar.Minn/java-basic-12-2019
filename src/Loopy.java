import java.sql.SQLOutput;

public class Loopy {
    public static void main(String[] args) {

        int[] iArr = { 6, 2, 3, 4 };
        int currentIndex = 0;

        while (currentIndex < iArr.length) {
            System.out.println(iArr[currentIndex]);
            currentIndex++;
        }


        for(int i = 3; i < iArr.length; i++) {
            System.out.println(iArr[i]);
        }

        System.out.println("For each Loop:");

        for(int tmpNumber : iArr) {
            System.out.println(tmpNumber);
        }

        // Topelt-cities:

        String[][] countryCity123 = {
                { "Tallinn", "Tartu", "Võru" },
                { "Stockholm", "Uppsala", "Köping" },
                { "Helsinki", "Espoo", "Hanko" },
        };

        System.out.println(countryCity123[1][2]);

        for(String[] currentCountryCity : countryCity123) {
            for(String city : currentCountryCity) {
                System.out.println(city);
            }
        }

        // Exercise 11
        System.out.println("Siin on 11. Harjutus:");

        int number11 = 1;
        while (number11 < 101) {
            System.out.println(number11);
            number11++;
        }

        // Exercise 12
        System.out.println("Siin on 12. Harjutus:");

        for (int number12 = 1; number12 < 101; number12++) {
            System.out.println(number12);
        }


        // Exercise 13
        System.out.println("Siin on 13. Harjutus:");

        // Defineerime massiivi:
        int[] number13 = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 0 };

        for ( int nOut : number13 ) {
            System.out.println(nOut);
        }


        // Exercise 14
        System.out.println("Siin on 14. Harjutus:");

        for (int number14 = 1; number14 < 101; number14++) {

            if (number14 % 3 == 0) {
                System.out.println(number14);
            }
        }


        // Exercise 15
        System.out.println("Siin on 15. Harjutus:");

        String[] bands = { "Sun", "Metsatöll", "Queen", "Metallica" };
        String commaBands = "";

        for (int bandNumber = 0; bandNumber < bands.length; bandNumber++) {
            commaBands = commaBands + bands[bandNumber];

            if (bandNumber < bands.length - 1) {
                commaBands = commaBands + ", ";
            }
        }

        System.out.println(commaBands);

        // Exercise 16 (eelmisest jätkuvalt):
        // String[] bands = { "Sun", "Metsatöll", "Queen", "Metallica" };
        System.out.println("Siin on 16. Harjutus:");

        for (int bandCounter = bands.length - 1 ; bandCounter >= 0 ; bandCounter--) {
            commaBands = commaBands + bands[bandCounter];

            if (bandCounter > 0) {
                commaBands = commaBands + ", ";
            }
        }

        System.out.println(commaBands);



        // Tsükkel:
        System.out.println("Siin on lihtne tsükkel:");

        for (int tsNumber = 27; tsNumber < 34; tsNumber++) {
            System.out.println(tsNumber);
        }

        // Harjutus 19:
        System.out.println("Teeme mingisuguse mustri...");

        String hash1 = "#+#+#+#+#+#+#+#+#+#";
        String plus1 = "+#+#+#+#+#+#+#+#+#+";

        for (int muster1 = 1 ; muster1 < 9 ; muster1++) {

            if (muster1 % 2 == 1) {
                System.out.println(hash1);
            } else {
                System.out.println(plus1);
            }

        };

        // Ex. 19 alternatiiv:
        System.out.println("Siin on harjutus 19 alternatiiv:");

        for(int jada = 0; jada < 8; jada++) {
            for (int sada = 0 ; sada < 19 ; sada++ ) {
                if(sada % 2 == 0) {

                    if(jada % 2 == 0) {
                        System.out.print("#");
                    } else {
                        System.out.print("+");
                    }

                } else {
                    if(jada % 2 == 1) {
                        System.out.print("+");
                    } else {
                        System.out.print("#");
                    }
                }
            }
            System.out.println();
        }



    }
}

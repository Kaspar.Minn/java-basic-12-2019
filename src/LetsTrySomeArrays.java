import java.util.Arrays;

import static java.util.Arrays.deepToString;

public class LetsTrySomeArrays {
    public static void main (String[] args) {

        int[] arrInts;
        arrInts = new int[5];
        arrInts[0] = 1;
        arrInts[1] = 2;
        arrInts[2] = 3;
        arrInts[3] = 4;
        arrInts[4] = 5;

        System.out.println(arrInts[0]);
        System.out.println(arrInts[1]);
        System.out.println(arrInts[2]);
        System.out.println(arrInts[3]);
        System.out.println(arrInts[4]);

        System.out.println(arrInts[arrInts.length - 3]);


        String[] arrCities = {"Tallinn", "Helsinki", "Madrid", "Paris"};
        arrCities[2] = ("Copenhagen");

        System.out.println(arrCities[2]);


        int[][] numbersArr = new int[3][];
        numbersArr[0] = new int[]{1, 2, 3};
        numbersArr[1] = new int[]{4, 5, 6};
        numbersArr[2] = new int[]{7, 8, 9, 0};

        System.out.println(numbersArr[2][1]);

        String[][] eeLinn = new String[2][2];

        String[][] arrCitCount = { {"Tallinn", "Tartu", "Valga", "Võru"}, {"Stockholm", "Uppsala", "Lund", "Köping"}, {"Value"} };
        arrCitCount[2] = new String[]{"Helsinki", "Espoo", "Hanko", "Jämsä"};

        System.out.println(deepToString(arrCitCount));



    }
}

import java.util.*;


public class Collections {
    public static void main(String[] args) {

        System.out.println("Exercise 20:");
        List<String> citiesOfWorld = new ArrayList<>();

        citiesOfWorld.add("Barcelona");
        citiesOfWorld.add("Birmingham");
        citiesOfWorld.add("Brussels");
        citiesOfWorld.add("Beirut");
        citiesOfWorld.add("Baden-Baden");
        citiesOfWorld.add("Bratislava");

        System.out.println(citiesOfWorld.get(2));
        System.out.println(citiesOfWorld.get(4));


        /// Set, unikaalne list elementidest:
        System.out.println("Defineerime Tree-Seti, vist...");


        Set<String> sampleSentence = new HashSet<String>();
        sampleSentence.add("This ");
        sampleSentence.add("is ");
        sampleSentence.add("a ");
        sampleSentence.add("sample ");
        sampleSentence.add("sentence.");

        System.out.println(sampleSentence);
        System.out.println("Selles setis oli " + sampleSentence.size() + " sõna.");

        // Lambi üritused...

        sampleSentence.forEach(name -> System.out.print("Eeeuh..."));
        sampleSentence.forEach(System.out::println);

        // Teeme hashmäppi...
        System.out.println("Mõned maailma maad ja linnad!!!");

        Map<String, String[]> linnadMaa = new HashMap<>();

        linnadMaa.put("Estonia", new String[] { "Tallinn", "Tartu" } );
        linnadMaa.put("Swööden", new String[] { "Uppsala", "Gothenburg" } );
        linnadMaa.put("Norway", new String[] { "Bergen", "Lillehammer" } );

        for (String nameKey : linnadMaa.keySet()) {
            System.out.println(nameKey + " linnad on: ");

            for (String cityKey : linnadMaa.get(nameKey)) {
                System.out.println("\t" + cityKey);
            }
        }


    }
}

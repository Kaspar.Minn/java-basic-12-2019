public class ProgrammingBasicsEx1 {
    public static void main (String[] args) {

        double number1 = 456.78;
        System.out.println(number1);

        String myText = "See tekst on kõik üks string!";
        System.out.println(myText);

        boolean myBooBooValue = true;
        System.out.println(myBooBooValue);

        char myCharA = 'a';
        System.out.println(myCharA);


        int number = 6;
        String can3 = (number % 3 == 0) ?
                "Yes, it divides!" :
                "No, it does not divide...";

        System.out.println(can3);

        String city = "Berlin";
        if (city.equals("Milano")) {
          System.out.println("It's nice and warm...");
        } else {
            System.out.println("Brr... it\'s so cold!");
        }

    // Siin on kommentaari kodu.

        int hinne = 2;
        if (hinne <= 1) {
            System.out.println("Failed!!!");
        } else if (hinne <= 2) {
            System.out.println("Not passable.");
        } else if (hinne <= 3) {
            System.out.println("Passable... maybe!");
        } else if (hinne <= 4) {
            System.out.println("Kind of good!");
        } else {
            System.out.println("Excellent!");
        }

        String gradeN =  args[1];
        int grade = Integer.parseInt(gradeN);
        if (grade > 90) {
            System.out.print("Ayy!");
        } else {
            System.out.println("No!!!");
        }

        switch (grade) {
            case 3:
                System.out.println("Ayy!");
                break;

            case 4:
                System.out.println("Nooo!!!");
                break;
        }

        int age = Integer.parseInt(args[2]);
        String ageText = (age < 88) ?
                "Reaalselt Noor!" :
                "Hingelt Noor!";
        System.out.println(ageText);

        int vanus = Integer.parseInt(args[3]);
        String vanur = (vanus > 88) ? ("So damn old.") : (age == 100 ? "Aaaalmost old..." : "For-eeeeever young!");

        System.out.println(vanur);

        // Comment.

    }
}

import java.util.ArrayList;
import java.util.List;

public class EX1 {
    public static void main(String[] args) {

        System.out.println("Exercise 1:");

        CountryInfo country1 = new CountryInfo();
        country1.countryName = "Estonia";
        country1.cities = new String[]{ "Tallinn", "Tartu" };
        country1.population = 10;


        CountryInfo country2 = new CountryInfo();
        country2.countryName = "Swööden";
        country2.cities = new String[]{ "Stockholm", "Gothenburg" };
        country2.population = 20;

        CountryInfo country3 = new CountryInfo();
        country3.countryName = "Finland";
        country3.cities = new String[]{ "Helsinki", "Hanko" };
        country3.population = 30;

        List<CountryInfo> countries = new ArrayList<>();
        countries.add(country1);
        countries.add(country2);
        countries.add(country3);

    }
}

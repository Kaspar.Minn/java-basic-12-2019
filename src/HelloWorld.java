public class HelloWorld {
    public static void main (String[] args) {
        System.out.println("Hello, World!!!");
        System.out.println("Käsurea parameeter: " + args[0]);

        int myNumber = 5;
        System.out.println(myNumber);

        byte mySmallNumber = (byte) 654;
        System.out.println(mySmallNumber);

        boolean isItTrue = true;
        System.out.println(isItTrue);

        double myDecimalNumber = 3.14;
        System.out.println(myDecimalNumber);

        double d1 = 5.6;
        double d2 = 5.8;
        double d3 = d1 + d2;
        System.out.println(d3);

        double d4 = (d1 * d2) - d3;
        System.out.println(d4);

        float myFloat = 6.7f;
        long myLargeNumber = 56000000000L;

        char myChar = 'A';
        System.out.println(myChar);

        char myChar2 = 'H';
        System.out.println((byte)myChar2);

        byte mySmallNumber2 = 78;
        System.out.println((char)mySmallNumber2);


    }
}

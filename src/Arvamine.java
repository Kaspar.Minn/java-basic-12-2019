import java.sql.SQLOutput;
import java.util.InputMismatchException;
import java.util.Scanner;

public class Arvamine {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        /*
        System.out.println("Palun ütle midagi:");
        String userText = scanner.nextLine();

        System.out.println("Kasutaja ütles nii:");
        System.out.println(userText);
        */

        int randomNumber = (int)(Math.random() * 5 + 1);

        System.out.println("Guess the number between 1 to 5!");
        int userNumber = 0;
        int guessCount = 0;

        do {
            guessCount++;
            userNumber = scanner.nextInt();
            if (userNumber == randomNumber) {
                System.out.println("Correct! You took " + guessCount + " times to guess the number!");
            } else if (userNumber > randomNumber) {
                System.out.println("OOF! That's too big! Try again!");
            } else {
                System.out.println("Oh no no no - that's too small! Try again!");
            }
        } while (userNumber != randomNumber);
    }

    private static int getNumberFromConsole(Scanner scanner) {
        while (true) {
            try {
                return scanner.nextInt();
            } catch (InputMismatchException e) {
                System.out.println("Your input was not a number. Please try again!");
                scanner.nextLine();
            }
        }
    }
}
